SKELETON PROJECT
=======================

Django Project with CMS 3 with frontend editing. This README, fabfile.py, project_env.py must be adapted to a new Project.

Installation Workflow:
======================

The installation procedure:

::
        # Fork the jnd_skeleton for your [new_project]
	# git clone https://jefferson_notaro@bitbucket.org/jefferson_notaro/[new_project].git
		Get requeriments, the main Wep package for [new_project]  and manage.py
	# Move inside the folder [new_project] and:
		2.1 virtualenv env
		2.2 source env/bin/active
		2.3 pip install -r requirements/dev.txt
			This will take a couple of minutes.
	# Create a mysql DB called db_[new_project]. The details for connection are in web/settings/dev.py
	# The Django environment:
		3.1 python manage.py syncdb --all
		3.2 python manage.py migrate --fake
		3.3 python manage.py runserver

