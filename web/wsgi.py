"""
WSGI config for seelenbalsamweb project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""

import os
import socket

hostname = socket.gethostname()
_SETTINGS = "jeffersonnotaroweb.settings.dev"
if hostname != 'Jeffersons-MacBook-Pro.local':
    _SETTINGS = "jeffersonnotaroweb.settings.prod"

os.environ.setdefault("DJANGO_SETTINGS_MODULE", _SETTINGS)

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
