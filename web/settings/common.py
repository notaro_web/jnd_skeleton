# -*- coding: utf-8 -*-
import os
gettext = lambda s: s
"""
Django settings for seelenbalsamweb project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
#import pdb
#pdb.set_trace()

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '-wtl4g*a_hks&p*gua=9&t*scs#jf^!#@=x)zl0zz_zl-rl613ETg@Wwe23%-'


ALLOWED_HOSTS = ['localhost', '127.0.0.1', 'jeffersonnotaro.ch', 'dev.jeffersonnotaro.ch', 'www.jeffersonnotaro.ch']


# Application definition


ROOT_URLCONF = 'jeffersonnotaroweb.urls'

WSGI_APPLICATION = 'web.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases



# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'de'

TIME_ZONE = 'Europe/Zurich'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, '..', 'media')
STATIC_ROOT = os.path.join(BASE_DIR, '..', 'static')

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)


################### DJANGO PIPELINE ######################

STATICFILES_STORAGE = 'pipeline.storage.PipelineCachedStorage'

# STATICFILES_FINDERS = (
#      'django.contrib.staticfiles.finders.FileSystemFinder',
#      'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#      'pipeline.finders.PipelineFinder',
#  )
STATICFILES_FINDERS = (
    'pipeline.finders.FileSystemFinder',
    'pipeline.finders.AppDirectoriesFinder',
    'pipeline.finders.PipelineFinder',
    'pipeline.finders.CachedFileFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

#
PIPELINE_CSS = {
     'main_css': {
         'source_filenames': (
           'css_src/normalize.min.css',
           'css_src/base.scss',
          # 'css_src/colors/*.css',
          # 'css_src/layers.css'
         ),
         'output_filename': 'css/jn_base.css',
         'extra_context': {
             'media': 'screen,projection',
         },
     },
     'print': {
         'source_filenames': (
           'css_src/print.scss',
         ),
         'output_filename': 'css/jn_print.css',
         'extra_context': {
             'media': 'print',
         },
     },
 }

PIPELINE_JS = {
     'main_js': {
         'source_filenames': (
           #'js_src/packages/jquery-1.11.2.min.js',
           'js_src/packages/vendor/modernizr-2.6.2-respond-1.1.0.min.js',
           'js_src/packages/angular.min.js',
           #'js_src/collections/*.js',
           #'js_src/plugins.js',
           #'js_src/base.js',
         ),
         'output_filename': 'js/jn_main.js',
     },
    'footer_js': {
         'source_filenames': (
           'js_src/packages/jquery-1.11.2.min.js',
           #'js_src/packages/vendor/modernizr-2.6.2-respond-1.1.0.min.js',
           #'js_src/collections/*.js',
           'js_src/plugins.js',
           'js_src/base.js',
         ),
         'output_filename': 'js/jn_footer.js',
     }
 }
PIPELINE_CSS_COMPRESSOR = 'pipeline.compressors.yuglify.YuglifyCompressor'
PIPELINE_JS_COMPRESSOR = 'pipeline.compressors.yuglify.YuglifyCompressor'
PIPELINE_COMPILERS = (
  'pipeline.compilers.sass.SASSCompiler',
)
################### PIPELINE END #########################

SITE_ID = 1

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.admindocs.middleware.XViewMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
    'pipeline.middleware.MinifyHTMLMiddleware',
)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors':
                (
                    'django.contrib.auth.context_processors.auth',
                    'django.template.context_processors.debug',
                    'django.template.context_processors.i18n',
                    'django.template.context_processors.media',
                    'django.template.context_processors.static',
                    'django.template.context_processors.tz',
                    'django.template.context_processors.csrf',
                    'django.template.context_processors.request',
                    'django.contrib.messages.context_processors.messages',
                    'sekizai.context_processors.sekizai',
                    'cms.context_processors.cms_settings',
                )
        }
    },
]

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'jeffersonnotaroweb', 'templates'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'django.contrib.staticfiles',
    'django.contrib.messages',
    'cms',
    'mptt',
    'menus',
    #'south',
    'sekizai',

     'djangocms_admin_style',
     'djangocms_text_ckeditor',
     #'djangocms_style',
     #'djangocms_column',
     #'djangocms_file',
     #'djangocms_flash',
     #'djangocms_googlemap',
     #'djangocms_inherit',
     'djangocms_link',
     #'djangocms_picture',
     #'djangocms_teaser',
     #'djangocms_video',
    # 'reversion',
    'jeffersonnotaroweb',
    #'shop',
#     'shop.addressmodel',
    'hvad',
    'international',
    #'registration',
    #'reservations',
    'treebeard',
#    'sentry',
    'pipeline',
    #'djangular',
    #'filer',
    #'image_gallery',
    'easy_thumbnails',
)
SOUTH_MIGRATION_MODULES = {
    'image_gallery': 'image_gallery.south_migrations',
}
LANGUAGES = (
    ## Customize this
    ('de', gettext('deutsch')),
    ('en', gettext('english')),
    ('es', gettext('castellano')),
    ('pt', gettext('português')),
)

CMS_LANGUAGES = {
    ## Customize this
    'default': {
        'public': True,
        'hide_untranslated': False,
        'redirect_on_fallback': True,
    },
    1: [
        {
            'public': True,
            'code': 'de',
            'hide_untranslated': False,
            'name': gettext('de'),
            'redirect_on_fallback': True,
        },
    ],
    2: [
        {
            'public': True,
            'code': 'en',
            'hide_untranslated': False,
            'name': gettext('en'),
            'redirect_on_fallback': True,
        },
    ],
    3: [
        {
            'public': True,
            'code': 'es',
            'hide_untranslated': False,
            'name': gettext('es'),
            'redirect_on_fallback': True,
        },
    ],
    4: [
        {
            'public': True,
            'code': 'pt',
            'hide_untranslated': False,
            'name': gettext('pt'),
            'redirect_on_fallback': True,
        },
    ],
}

CMS_TEMPLATES = (
    ## Customize this
    ('fullwidth.html', 'Fullwidth'),
    ('sidebar_left.html', 'Sidebar Left'),
    ('sidebar_right.html', 'Sidebar Right'),
    ('article_sidebar_right.html', 'Article Sidebar Right')
)

CMS_PERMISSION = True

CMS_PLACEHOLDER_CONF = {}

DATABASES = {
    'default':
        {'ENGINE': 'django.db.backends.sqlite3', 'NAME': 'project.db', 'HOST': 'localhost', 'USER': '', 'PASSWORD': '', 'PORT': ''}
}

# RESERVATION
RESERVATION_SPOTS_TOTAL = 32
APP_SHORTNAME = 'Jefferson Notaro'
APP_URL = 'localhost:8000/'
EMAIL_FROM = 'test@gmail.com'
BOOKING_TIME_INTERVAL = 'day'

#SCHEDULE
FIRST_DAY_OF_WEEK = 1

#FOR REGISTRATION
ACCOUNT_ACTIVATION_DAYS = 7 # One-week activation window; you may, of course, use a different value.

