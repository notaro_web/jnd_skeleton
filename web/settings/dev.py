from __future__ import absolute_import
from .common import *

# Dev overrides
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = DEBUG
#PIPELINE = True

INSTALLED_APPS += (
    'debug_toolbar',
    'debugtools',
    'django_nose',
#    'sentry',
)

DATABASES = {
    'default':
        {'ENGINE': 'django.db.backends.mysql',
         'NAME': 'db_jeffersonnotaro',
         'HOST': '127.0.0.1',
         'USER': 'root',
         'PASSWORD': ''
        }
}

#EMAIL DEV
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'jefferson.notaro.emailtest@gmail.com'
EMAIL_HOST_PASSWORD = ''
DEFAULT_FROM_EMAIL = 'jefferson.notaro@gmail.com'