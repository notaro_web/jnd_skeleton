from django.conf.urls import *  # NOQA
from django.conf.urls.i18n import i18n_patterns
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from django.conf import settings
from cms.sitemaps import CMSSitemap
from .views import CategoryView, ProjectView, ThanksView, ProjectDetailView


admin.autodiscover()

urlpatterns = i18n_patterns('',
    url(r'^admin/', include(admin.site.urls)),  # NOQA
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap',
        {'sitemaps': {'cmspages': CMSSitemap}}),
    #url(r'^kontakt/$', ContactView.as_view(), name='contact_form'),
    url(r'^danke/$', ThanksView.as_view(), name='thank_you'),
    url(r'^projects/$', ProjectView.as_view(), name='jn_project' ),
    #url(r'^(?P<slug>[-\w]+)/$', ProjectView.as_view(), name='jn_project'),
    #url(r'^project/(?P<slug>[-\w]+)$', ProjectViewDetail.as_view(), name='jn_project_detail' ),
    url(r'^project/(?P<category>[-\w]+)/(?P<slug>[-\w]+)/$', ProjectDetailView.as_view(), name='jn_project_detail'),
    #url(r'^(?P<parent_slugs>([-\w]+/)*)?(?P<slug>[-\w]+)/$', CategoryView.as_view(), name='jn_category'),
    url(r'^', include('cms.urls')),
)

# This is only needed when using runserver.
if settings.DEBUG:
    urlpatterns = patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',  # NOQA
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        ) + staticfiles_urlpatterns() + urlpatterns  # NOQA
