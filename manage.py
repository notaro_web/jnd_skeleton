#!/usr/bin/env python
import os
import sys
import socket

if __name__ == "__main__":
    hostname = socket.gethostname()
    _SETTINGS = "jeffersonnotaroweb.settings.dev"
    if hostname != 'Jeffersons-MacBook-Pro.local':
    	_SETTINGS = "jeffersonnotaroweb.settings.prod"

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", _SETTINGS)

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
